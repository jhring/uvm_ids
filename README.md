# UVM IDS

Software repository for an upcoming paper:
*Methods for Host-Based Intrusion Detection with Deep Learning*.

This core of this project is the development of Host-Based Intrusion Detection systems (HIDs) using artificial neural networks.
HIDS automatically detect events that indicate compromise by adversarial applications.
We preform anomaly detection on sequences of system calls by learning the baseline language model and detecting deviations.

## Setup
We recommend using the Anaconda Python Distribution.
Our Conda environment is specified in `environment.yml`.

## Data Sets
We present a new system call data set, Plaid Lab Artificial Intrusion Dataset (PLAID) included in this repository.
Simply decompress `data/PLAID.tar.xz`.
We additionally utilized the existing dataset ADFA-LD. Running `adfa_preprocessing.py` will download and decode ADFA-LD.
Finally, the exact data splits used in our analysis are presented in `out/`.

## Model Training and Prediction
### Running on DeepGreen
Executing `scripts/run_trials.py` on a user node submits a SLURM job for each replicate trials for all model configurations.
In total 540 jobs will be submitted (3 model architectures x 3 configurations each x 2 data sets x 30 replicate trials).
### Manual
Alternatively, a robust CLI for training any of our individual model configurations or ensembles is provided in `src/train_ensemble.py`.

## Visualizations
Source code for our plots and tables in provided in `viz`. 
These files primarily operate on the output of the automated model training and prediction script.
An exception is `dataset_plots.py` which only requires the existence of the previously discussed datasets

