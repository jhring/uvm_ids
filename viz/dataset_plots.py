#!/usr/bin/env python3
# -*- coding: utf-8 -*

"""Creates plots of system call language scaling and divergence

    Computes n-grams for the system call datasets and visualizes the rank frequency scaling of these n-grams as well
    as the divergence between attack and baseline sequences.

"""

from collections import Counter
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import distance
from sklearn.linear_model import HuberRegressor


def count_system_calls(file_path, n):
    """Counts system call n-grams

    Parameters
    ----------
    file_path : pathlib.Path
        Path to system call text files
    n : int
        Sequence length for n-grams

    Returns
    -------
    counts : Counter
        Frequency of each observed system call n-gram

    """
    counts = Counter()
    text_files = file_path.rglob("*.txt")

    for files in text_files:
        with open(files) as f:
            line = f.readline()
            seq = line.split(" ")
            if 4495 >= len(seq) >= 8:
                grams = get_ngrams(seq, n)
                grams = [" ".join(x) for x in grams]
                counts.update(grams)

    return counts


def make_rank_freq(attack, base, out_path=None):
    """Makes a rank frequency plot of attack v. baseline n-grams

    Parameters
    ----------
    attack : Counter
        Frequencies of attack n-grams
    base : Counter
        Frequencies of baseline n-grams
    out_path : str
        Path to save plot. Plot is displayed if None.

    """
    gram = len(list(attack.keys())[0].split())
    attack = np.array(sorted(list(attack.values()), reverse=True), dtype=float)
    base = np.array(sorted(list(base.values()), reverse=True), dtype=float)

    base_x = np.arange(len(base))
    base_y = np.log10(base)

    attack_x = np.arange(len(attack))
    attack_y = np.log10(attack)

    if gram == 1:
        model = HuberRegressor()
        model.fit(base_x.reshape(-1, 1), base_y)
        x_plot = np.linspace(base_x.min(), base_x.max())
        y_plot = model.predict(x_plot[:, np.newaxis])
        plt.plot(
            x_plot,
            y_plot,
            c="firebrick",
            label=f"$y = 10^{{{model.coef_[0]:0.4f}x + {model.intercept_:0.4f}}}$",
        )

        model = HuberRegressor()
        model.fit(attack_x.reshape(-1, 1), attack_y)
        x_plot = np.linspace(attack_x.min(), attack_x.max())
        y_plot = model.predict(x_plot[:, np.newaxis])
        plt.plot(
            x_plot,
            y_plot,
            c="indigo",
            label=f"$y = 10^{{{model.coef_[0]:0.4f}x + {model.intercept_:0.4f}}}$",
        )
        plt.xlabel(f"Rank of System Call {gram}-grams")
    else:
        base_x = np.log10(base_x + 1)
        attack_x = np.log10(attack_x + 1)
        plt.xlabel(f"$\log_{10}$ Rank of System Call {gram}-grams")

    plt.plot(base_x, base_y, label="Baseline")
    plt.plot(attack_x, attack_y, label="Attack")

    plt.xlabel(f"Rank of System Call {gram}-grams")
    plt.ylabel(r"$\log_{10}$ Frequency")
    plt.legend()

    if out_path is None:
        plt.show()
    else:
        plt.savefig(out_path)
    plt.close()


def get_ngrams(sequence, n):
    """Computes n-grams from a sequence

    Parameters
    ----------
    sequence : List[T]
    n : int

    Returns
    -------
    n_gams : List[List[T]]

    """
    return zip(*[sequence[i:] for i in range(n)])


def bar_plot(attack, base, out_path=None):
    """Creates a up-down bar plot of system call frequencies

    Parameters
    ----------
    attack : Counter
        Attack system call frequencies
    base : Counter
        Baseline system call frequencies
    out_path : str
        Path to save plot. Plot is displayed if None.

    """
    syscalls = list(set(list(attack.keys()) + list(base.keys())))
    attack_freqs = np.zeros(len(syscalls), dtype=float)
    base_freqs = np.zeros(len(syscalls), dtype=float)
    for idx, syscall in enumerate(syscalls):
        attack_freqs[idx] = attack[syscall]
        base_freqs[idx] = base[syscall]

    base_freqs, attack_freqs, syscalls = zip(
        *sorted(zip(base_freqs, attack_freqs, syscalls), reverse=True)
    )

    attack_freqs = np.log10(np.array(attack_freqs) + 1) * -1
    base_freqs = np.log10(np.array(base_freqs) + 1)

    fig, ax = plt.subplots(figsize=(15, 10), dpi=300)
    ax.bar(syscalls, base_freqs, label="Baseline")
    ax.bar(syscalls, attack_freqs, label="Attack")

    plt.xticks(rotation=90)
    plt.tight_layout()

    ticks = ax.get_yticks()
    ax.set_yticklabels([int(abs(tick)) for tick in ticks])
    plt.xticks(fontsize=6)
    plt.legend()
    plt.ylabel(r"$\log_{10}$ Frequency")
    plt.xlabel("System Call")
    if out_path is None:
        plt.show()
    else:
        plt.savefig(out_path)
    plt.close()


if __name__ == "__main__":
    plaid_root = Path("../data/PLAID")
    adfa_root = Path("../data/ADFA_decoded_i386")

    out_path = Path("../plots/datasets")
    out_path.mkdir(parents=True, exist_ok=True)

    for gram in range(1, 4):
        p_attack = count_system_calls((plaid_root / "attack"), gram)
        p_baseline = count_system_calls((plaid_root / "baseline"), gram)

        adfa_attack = count_system_calls((adfa_root / "Attack_Data_Master"), gram)
        adfa_base = count_system_calls(
            (adfa_root / "Training_Data_Master"), gram
        ) + count_system_calls((adfa_root / "Validation_Data_Master"), gram)

        for attack, base, label in zip(
            [adfa_attack, p_attack], [adfa_base, p_baseline], ["adfa", "plaid"]
        ):

            make_rank_freq(
                attack, base, out_path=out_path / f"{label}_{gram}gram_rank_freq.png"
            )

            if gram == 1:
                bar_plot(attack, base, out_path=out_path / f"{label}_bar.png")

                p_1, p_2 = zip(
                    *[
                        (attack[x], base[x])
                        for x in set(list(attack.keys()) + list(base.keys()))
                    ]
                )
                print(f"{label} JSD: ", distance.jensenshannon(p_1, p_2))
