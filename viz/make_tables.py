#!/usr/bin/env python3
# -*- coding: utf-8 -*

"""Computes values for and output's Latex tables comparing models and ensembles

    Tables compare number of parameters, training time, evaluation time, trace and application level AUC and
    FPR @ TPR=1. All stats are reported as the mean plus/ minus standard deviation of all replicate trials.
    Data is pulled from training log files and saved models.
"""

import datetime
from pathlib import Path

import numpy as np
import pandas as pd
from scipy.stats import ttest_ind
from tensorflow.keras.models import load_model

from roc_plots import get_agg_scores, get_roc_vals, get_y_vals, load_scores


def get_training_time(model_arch, model_idx, data_set):
    """Get the mean training time plus/ minus standard deviation for the specified model training run.

    Parameters
    ----------
    model_arch : {"cnnrnn", "lstm", "wavenet"}
    model_idx : {1, 2, 3}
    data_set : {"adfa", "plaid"}

    Returns
    -------
    ret : str
        Latex string of training time plus/ minus standard deviation for use in table

    """
    epoch = 300 if data_set == "adfa" else 30
    logs = Path(f"../trials/{model_arch}_{data_set}_{epoch}").glob(
        f"log_{model_idx}_*.log"
    )
    training_times = []
    for log in logs:
        df = pd.read_csv(log)
        df = df[df["epoch"] < 300]
        training_times.append(df["time"].sum())

    training_time = round(np.mean(training_times))
    training_time = str(datetime.timedelta(seconds=training_time))
    std = str(datetime.timedelta(seconds=round(np.std(training_times))))[3:]
    ret = f"{training_time} $\\pm$ {std}"
    return ret


def get_eval_time(model_arch, model_idx, data_set):
    """Get the mean evaluation time plus/ minus standard deviation for the specified model training run.

    Parameters
    ----------
    model_arch : {"cnnrnn", "lstm", "wavenet"}
    model_idx : {1, 2, 3}
    data_set : {"adfa", "plaid"}

    Returns
    -------
    ret : str
        Latex string of evaluation time plus/ minus standard deviation for use in table

    """
    epoch = 300 if data_set == "adfa" else 30
    evals = Path(f"../trials/{model_arch}_{data_set}_{epoch}").glob(
        f"eval_{model_idx}_*.npz"
    )

    eval_times = [np.load(str(eval), allow_pickle=True)["arr_0"][2] for eval in evals]
    std = np.std(eval_times)
    space = "" if std >= 10 else "\\ \\ "

    return f"{np.mean(eval_times):0.1f} $\\pm$ {space} {std:0.1f}"


def get_params(model_arch, model_idx, data_set):
    """Get the number of parameters in the specified model configuration

    Parameters
    ----------
    model_arch : {"cnnrnn", "lstm", "wavenet"}
    model_idx : {1, 2, 3}
    data_set : {"adfa", "plaid"}

    Returns
    -------
    ret : str
        number of parameters

    """
    epoch = 300 if data_set == "adfa" else 30
    model_path = str(
        Path(f"../trials/{model_arch}_{data_set}_{epoch}/model_{model_idx}_00.ckpt")
    )
    model = load_model(model_path)
    return str(model.count_params())


def get_threshold_comp(scores, norm_y, y_agg, process_lens):
    """Creates latex strings for performance metrics and trace v. application comparison

    Parameters
    ----------
    scores : np.array
        Negative log-likelihood of a trace occurring
    norm_y : np.array
        True y labels for each trace
    y_agg : np.array
        True y labels at the application level
    process_lens : List[int]
        Number of traces from each application

    Returns
    -------
        reg_auc : str
            Latex string of trace level AUC plus/ minus standard deviation for use in table
        reg_fpr : str
            Latex string of trace level FPR @ TPR=1 plus/ minus standard deviation for use in table
        agg_auc : str
            Latex string of application level AUC plus/ minus standard deviation for use in table
        agg_fpr : str
            Latex string of application level FPR @ TPR=1 plus/ minus standard deviation for use in table

    """
    agg_fprs = []
    agg_aucs = []
    for score in scores:
        agg_scores = get_agg_scores(score, process_lens)
        fprs, tprs, aucs = get_roc_vals(y_agg, agg_scores)
        fprs = [x[np.argmax(y == 1)] for x, y in zip(fprs, tprs)]
        agg_fprs.append(np.mean(fprs))
        agg_aucs.append(np.mean(aucs))

    reg_fprs, reg_tprs, reg_aucs = get_roc_vals(norm_y, scores)
    reg_fprs = [x[np.argmax(y == 1)] for x, y in zip(reg_fprs, reg_tprs)]

    auc_p = ttest_ind(reg_aucs, agg_aucs)[1]
    fpr_p = ttest_ind(reg_fprs, agg_fprs)[1]

    auc_p = "^{\\dagger}" if auc_p < 0.001 else "\\ \\"
    fpr_p = "^{\\dagger}" if fpr_p < 0.001 else "\\ \\"
    reg_auc = f"${np.mean(reg_aucs):0.3f} \\pm {np.std(reg_aucs):0.3f}$"
    agg_auc = f"${np.mean(agg_aucs):0.3f}{auc_p} \\pm {np.std(agg_aucs):0.3f}$"
    reg_fpr = f"${np.mean(reg_fprs):0.3f} \\pm {np.std(reg_fprs):0.3f}$"
    agg_fpr = f"${np.mean(agg_fprs):0.3f}{fpr_p} \\pm {np.std(agg_fprs):0.3f}$"
    return reg_auc, reg_fpr, agg_auc, agg_fpr


def make_table():
    """Writes model, number of parameters, training time, evaluation time, trace and application level AUC and
    FPR @ TPR=1. All stats are reported as the mean plus/ minus standard deviation of all replicate trials.
    """
    model_archs = ["cnnrnn", "lstm", "wavenet"]
    model_labels = ["CNN/RNN", "LSTM", "WaveNet"]
    data_sets = ["adfa", "plaid"]
    table_str = " & ".join(
        [
            "Model",
            "Parameters",
            "Training Time",
            "Eval. Time",
            "AUC Trace",
            "FPR Trace",
            "AUC Program",
            "FPR Program",
        ]
    )
    print(table_str)
    for data_set in data_sets:
        norm_y, y_agg, process_lens = get_y_vals(data_set)
        for model_arch, model_label in zip(model_archs, model_labels):
            for idx in range(3):
                scores = load_scores(model_arch, data_set, idx)[0]
                row = [
                    f"{model_label}$_{idx}$",
                    get_params(model_arch, idx, data_set),
                    get_training_time(model_arch, idx, data_set),
                    get_eval_time(model_arch, idx, data_set),
                    *get_threshold_comp(scores, norm_y, y_agg, process_lens),
                ]
                row[-1] += " \\\\"
                print(" & ".join(row))


def make_ensemble_table():
    """Writes ensemble, trace and application level AUC and FPR @ TPR=1. All stats are reported as the mean plus/ minus
    standard deviation of all replicate trials.
    """
    model_archs = ["cnnrnn", "lstm", "wavenet"]
    model_labels = ["CNN/RNN", "LSTM", "WaveNet"]
    data_sets = ["adfa", "plaid"]

    table_str = " ".join(
        [
            "Model",
            "AUC Trace",
            "FPR Trace",
            "AUC Program",
            "FPR Program",
        ]
    )
    print(table_str)

    def print_row(model_name, scores, norm_y, y_agg, process_lens):
        scores = np.mean(np.stack(scores), axis=0)
        row = [
            model_name,
            *get_threshold_comp(scores, norm_y, y_agg, process_lens),
        ]
        row[-1] += " \\\\"
        print(" & ".join(row))

    for data_set in data_sets:
        norm_y, y_agg, process_lens = get_y_vals(data_set)
        for model_arch, model_label in zip(model_archs, model_labels):
            scores, relu_scores = zip(
                *[load_scores(model_arch, data_set, idx) for idx in range(3)]
            )
            print_row(f"Avg. {model_label}", scores, norm_y, y_agg, process_lens)
            print_row(f"Relu. {model_label}", relu_scores, norm_y, y_agg, process_lens)

        for idx in range(3):
            scores, relu_scores = zip(
                *[load_scores(model_arch, data_set, idx) for model_arch in model_archs]
            )
            print_row(f"Hybrid$_{idx}$", scores, norm_y, y_agg, process_lens)
            print_row(f"Relu. Hybrid$_{idx}$", relu_scores, norm_y, y_agg, process_lens)


def get_total_time():
    """Calculates the total training time of all replicate trials"""

    def aux(model_arch, model_idx, data_set):
        epoch = 300 if data_set == "adfa" else 30
        logs = Path(f"../trials/{model_arch}_{data_set}_{epoch}").glob(
            f"log_{model_idx}_*.log"
        )
        training_times = []
        for log in logs:
            df = pd.read_csv(log)
            df = df[df["epoch"] < 300]
            training_times.append(df["time"].sum())

        return np.sum(training_times)

    training_time = 0
    model_archs = ["cnnrnn", "lstm", "wavenet"]
    data_sets = ["adfa", "plaid"]
    for data_set in data_sets:
        for model_arch in model_archs:
            for idx in range(3):
                training_time += aux(model_arch, idx, data_set)
    print(str(datetime.timedelta(seconds=round(training_time))))


if __name__ == "__main__":
    make_table()
    make_ensemble_table()
