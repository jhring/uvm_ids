#!/usr/bin/env python3
# -*- coding: utf-8 -*

"""
    Generates ROC plots comparing various model configurations. Requires previously trained and evaluated models.
"""

from itertools import chain
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from scipy import interpolate
from sklearn import metrics

from data_processing import load_nested_test


def plot_roc_curve(model_scores, labels, y_true, out_path):
    """Plots ROC curves

    Parameters
    ----------
    model_scores : List[np.array]
        Negative log-likelihood of system call trace from each model.
    labels : List[str]
        Names for each model, used in legend
    y_true : np.array
        True classification of system call traces
    out_path : str
        Location to save plot

    """

    for model_score, label in zip(model_scores, labels):
        fprs, tprs, aucs = get_roc_vals(y_true, model_score, stacked=False)
        sns.lineplot(
            fprs,
            tprs,
            label=f"{label} AUC: {np.mean(aucs):0.3f} $\\pm$ {np.std(aucs):0.3f}",
            legend="full",
        )

    plt.plot([0, 1], [0, 1], color="navy", lw=2, linestyle="--")

    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.savefig(out_path)
    plt.close()


def get_roc_vals(y_true, scores, pos_label=1, stacked=True):
    """Computes ROC curve metrics

    Computes ROC curve metrics for each model's scores. Interpolates the metrics allowing various model's ROC curves
    to be plotting on the same axis.

    Parameters
    ----------
    y_true : np.array
        True system call trace classification
    scores : List[np.array]
        Negative log-likelihood of system call trace from each model.
    pos_label : int
        Label for attack traces
    stacked : bool
        Scores are nested at application level

    Returns
    -------
    fprs_total : List[np.array]
        False positive rates for each model
    tprs : List[np.array]
        Corresponding true positive rate for each model
    aucs : List[float]
        Area under the curve for each model

    """
    fprs = []
    tprs = []
    aucs = []
    for score in scores:
        fpr, tpr, _ = metrics.roc_curve(y_true, score, pos_label=pos_label)
        fprs.append(fpr)
        tprs.append(tpr)
        aucs.append(metrics.auc(fpr, tpr))
    fprs_total = np.sort(np.unique(np.concatenate(fprs)))
    tprs = [interpolate.interp1d(fpr, tpr)(fprs_total) for fpr, tpr in zip(fprs, tprs)]
    if stacked:
        tprs = np.stack(tprs)
        fprs_total = np.tile(fprs_total, (len(scores), 1))
    else:
        tprs = np.concatenate(tprs)
        fprs_total = np.tile(fprs_total, len(scores))
    tprs[fprs_total == 0] = 0

    return fprs_total, tprs, aucs


def get_agg_scores(scores, process_lens, trials=30):
    """Get scores nested at the application level

    Parameters
    ----------
    scores : np.array
        Negative log-likelihood of system call traces
    process_lens : List[int]
        Number of traces associated with each application
    trials : int
        Number of bootstrapping trials of the baseline sequences

    Returns
    -------
    ret: List[np.array]
        Scores aggregated at the application level for each trial

    """
    val_scores = scores[: len(scores) // 2]
    atk_scores = scores[len(scores) // 2 :]

    def aux(scores, shuffle=False):
        agg_scores = np.zeros(len(process_lens))
        if shuffle:
            np.random.shuffle(scores)

        offset = 0
        for idx, process_len in enumerate(process_lens):
            seqs = scores[offset : offset + process_len]
            agg_scores[idx] = np.median(seqs)
            offset += process_len

        return agg_scores

    atk_agg = aux(atk_scores)
    ret = [
        np.concatenate([aux(val_scores, shuffle=True), atk_agg]) for _ in range(trials)
    ]
    return ret


def load_scores(model_arch, data_set, model_idx):
    """Loads scores from previous model predictions

    Parameters
    ----------
    model_arch : {"cnnrnn", "lstm", "wavenet"}
    data_set : {"adfa", "plaid"}
    model_idx : {1, 2, 3}

    Returns
    -------
    reg_scores : List[np.array]
        Negative log-likelihood of test set system call traces from each replicate trial
    relu_scores : List[np.array]
        Negative log-likelihood of test set system call traces from each replicate trial using relu

    """
    epoch = 300 if data_set == "adfa" else 30
    path = Path(f"../trials/{model_arch}_{data_set}_{epoch}")
    score_paths = path.glob(f"eval_{model_idx}_*.npz")
    reg_scores = []
    relu_scores = []
    for score_path in score_paths:
        scores, baseline, _ = np.load(score_path, allow_pickle=True)["arr_0"]
        tmp = scores - baseline
        tmp = np.maximum(tmp, 0.001 * tmp)
        reg_scores.append(scores)
        relu_scores.append(tmp)
    return reg_scores, relu_scores


def get_singles(model_arch, data_set):
    """Loads scores from all configurations of a given model

    Parameters
    ----------
    model_arch : {"cnnrnn", "lstm", "wavenet"}
    data_set : {"adfa", "plaid"}

    Returns
    -------
    ret : List[np.array]
        Scores from the 3 model configurations

    """
    single_scores, relu_scores = zip(
        *[load_scores(model_arch, data_set, idx) for idx in range(3)]
    )
    avg_reg = np.mean(np.stack(single_scores), axis=0)
    relu = np.mean(np.stack(relu_scores), axis=0)
    return [*single_scores, avg_reg, relu]


def get_y_vals(data_set):
    """Loads the true classification for the given test set

    Parameters
    ----------
    data_set {"adfa", "plaid"}

    Returns
    -------
    norm_y : np.array
        Classification of system call traces
    y_agg : List[np.array]
        Classification of system call traces aggregated at the application level
    process_lens : List[np.array]
        Number of traces in each corresponding application

    """
    attack = load_nested_test(data_set)[1]
    process_lens = [len(x) for x in attack]

    y_agg = np.zeros(2 * len(attack))
    y_agg[len(attack) :] = 1

    attack = list(chain(*attack))
    norm_y = np.zeros(2 * len(attack))
    norm_y[len(attack) :] = 1

    return norm_y, y_agg, process_lens


def make_plots(data_set):
    """Generates all ROC comparison plots in the paper

    plots for each data set:
        - best.png Highest trace level AUC configuration for each model type
        - best_agg.png Highest application level AUC configuration for each model type
        - {cnnrnn, lstm, wavenet}_singles.png Comparison of model configurations within the same type
        - hybrid_singles Comparison of hybrid ensemble models

    Parameters
    ----------
    data_set : {"adfa", "plaid"}

    Returns
    -------

    """
    out_path = Path(f"../plots/roc_{data_set}/")
    out_path.mkdir(exist_ok=True, parents=True)

    norm_y, y_agg, process_lens = get_y_vals(data_set)

    best_singles = []
    best_single_labels = []
    all_ensembles = []
    all_ensemble_labels = []
    for model_arch, model_label in zip(
        ["cnnrnn", "lstm", "wavenet"], ["CNN_RNN", "LSTM", "WaveNet"]
    ):

        scores = get_singles(model_arch, data_set)
        best_idx = int(
            np.argmax(
                [np.mean(get_roc_vals(norm_y, x, stacked=False)[2]) for x in scores[:3]]
            )
        )

        labels = [f"{model_label}_{x}" for x in range(3)]
        labels.extend([f"{model_label} Avg. Ens.", f"{model_label} Relu Ens."])

        best_singles.append(scores[best_idx])
        best_single_labels.append(labels[best_idx])
        all_ensembles.extend(scores[3:])
        all_ensemble_labels.extend(labels[3:])

        plot_roc_curve(
            scores, labels, norm_y, str(out_path / f"{model_arch}_singles.png")
        )
    hybrids = []
    hybrid_labels = []
    for idx in range(3):
        scores, relu_scores = zip(
            *[
                load_scores(model_arch, data_set, idx)
                for model_arch in ["cnnrnn", "lstm", "wavenet"]
            ]
        )
        scores = np.mean(np.stack(scores), axis=0)
        relu_scores = np.mean(np.stack(relu_scores), axis=0)
        hybrids.extend([scores, relu_scores])
        hybrid_labels.extend([f"Hybrid$_{idx}$", f"Relu. Hybrid$_{idx}$"])
    plot_roc_curve(hybrids, hybrid_labels, norm_y, str(out_path / "hybrid_singles.png"))

    all_ensembles.extend(hybrids)
    all_ensemble_labels.extend(hybrid_labels)
    best_idx = int(
        np.argmax(
            [np.mean(get_roc_vals(norm_y, x, stacked=False)[2]) for x in all_ensembles]
        )
    )
    scores = [*best_singles, all_ensembles[best_idx]]
    labels = [*best_single_labels, all_ensemble_labels[best_idx]]
    plot_roc_curve(scores, labels, norm_y, str(out_path / "best.png"))

    for idx, model_scores in enumerate(scores):
        agg_scores = []
        for score in model_scores:
            agg_scores.extend(get_agg_scores(score, process_lens))
        scores[idx] = agg_scores
    plot_roc_curve(scores, labels, y_agg, str(out_path / "best_agg.png"))


if __name__ == "__main__":
    make_plots("plaid")
    make_plots("adfa")
