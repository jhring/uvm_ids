#!/usr/bin/env python3
# -*- coding: utf-8 -*

"""
    Compares final validation loss to models final performance.
"""

from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from roc_plots import get_roc_vals, get_y_vals, load_scores


def get_training_loss_auc(model_arch, model_idx, data_set, norm_y):
    """Get the validation loss and AUC for each trial of the specified model training run.

    For use in loss comp

    Parameters
    ----------
    model_arch : {"cnnrnn", "lstm", "wavenet"}
    model_idx : {1, 2, 3}
    data_set : {"adfa", "plaid"}
    norm_y: np.array
        True y values for trace level

    Returns
    -------
    val_loss : List[float]
        Validation loss for each replicate trial
    aucs : List[float]
        AUC for each replicate trial

    """
    epoch = 300 if data_set == "adfa" else 30
    logs = Path(f"../trials/{model_arch}_{data_set}_{epoch}").glob(
        f"log_{model_idx}_*.log"
    )
    val_loss = []
    for log in logs:
        df = pd.read_csv(log)
        df = df[df["epoch"] < 300]
        val_loss.append(df.loc[len(df) - 1, "val_loss"])

    val_loss = np.array(val_loss)
    scores = load_scores(model_arch, data_set, model_idx)[0]
    aucs = get_roc_vals(norm_y, scores)[2]

    return val_loss, aucs


def loss_comp():
    """Scatter plot of training loss v. AUC for all model configurations"""
    model_archs = ["cnnrnn", "lstm", "wavenet"]
    model_labels = ["CNN/RNN", "LSTM", "WaveNet"]
    data_sets = ["adfa", "plaid"]
    for data_set in data_sets:
        all_vals = []
        all_aucs = []
        model_nums = []
        norm_y = get_y_vals(data_set)[0]
        for (model_arch, model_label) in zip(model_archs, model_labels):
            for idx in range(3):
                val_loss, aucs = get_training_loss_auc(
                    model_arch, idx, data_set, norm_y
                )
                all_vals.extend(val_loss)
                all_aucs.extend(aucs)
                model_nums.extend([f"{model_label}$_{idx}$" for _ in range(30)])

        sns.scatterplot(x=all_vals, y=all_aucs, hue=model_nums, legend="full")
        plt.legend(fontsize="small")
        plt.xlabel("Validation Loss", fontsize="small")
        plt.ylabel("ROC AUC", fontsize="small")
        plt.savefig(f"../plots/{data_set}_loss_auc.png")
        plt.close()


if __name__ == "__main__":
    loss_comp()
